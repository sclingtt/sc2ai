from basic_agent import BasicAgent
from pysc2.env import sc2_env
from pysc2 import maps
from pysc2.lib import actions, features
from absl import app
from absl import flags
import sys

FLAGS = flags.FLAGS
flags.DEFINE_string("map", "MoveToBeacon", "Map to use")
FLAGS(sys.argv)


def _main(args):
    agent = BasicAgent()
    try:
        while 1:
            with sc2_env.SC2Env(
                map_name=FLAGS.map,
                players=[sc2_env.Agent(agent.race)],
                agent_interface_format=features.AgentInterfaceFormat(
                    feature_dimensions=features.Dimensions(screen=84, minimap=64)),
                visualize=True,
            ) as env:
                agent.setup(env.observation_spec(), env.action_spec())
                timesteps = env.reset()
                while 1:
                    step_actions = [agent.step(timesteps[0])]
                    if timesteps[0].last():
                        break
                    timesteps = env.step(step_actions)
    except KeyboardInterrupt:
        pass


if __name__ == "__main__":
    app.run(_main)
