from pysc2.agents import base_agent
from pysc2.lib import actions, features
from pysc2.env import sc2_env

class BasicAgent(base_agent.BaseAgent):

    @property
    def race(self):
        return sc2_env.Race.zerg

    def step(self, obs):
        super(BasicAgent, self).step(obs)
        return actions.FUNCTIONS.no_op()


